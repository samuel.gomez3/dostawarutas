package co.com.pragma.util;

public class Constantes {
	public static final String DIRECTIONS_API_BASE = "https://maps.googleapis.com/maps/api/directions/json";
	public static final String GEOCODING_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=";
}
