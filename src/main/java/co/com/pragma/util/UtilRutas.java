package co.com.pragma.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UtilRutas {
	public static List<List<String>> filtrarPermutacionesIniciales(List<String> lista, String primerElemento) {
	    List<List<String>> permutacionesIniciales = new ArrayList<>();
	    int index = lista.indexOf(primerElemento);
	    
	    if (index != -1) {
	        Collections.swap(lista, 0, index);
	        generarPermutaciones(lista.size() - 1, lista.subList(1, lista.size()), permutacionesIniciales);
	        for (List<String> permutacion : permutacionesIniciales) {
	            permutacion.add(0, primerElemento);
	        }
	    }
	    
	    return permutacionesIniciales;
	}

	private static void generarPermutaciones(int n, List<String> lista, List<List<String>> permutaciones) {
	    if (n == 1) {
	        permutaciones.add(new ArrayList<>(lista));
	        return;
	    }

	    for (int i = 0; i < n; i++) {
	        generarPermutaciones(n - 1, lista, permutaciones);
	        
	        if (n % 2 == 0) {
	            intercambiar(lista, i, n - 1);
	        } else {
	            intercambiar(lista, 0, n - 1);
	        }
	    }
	}

	private static void intercambiar(List<String> lista, int i, int j) {
	    String temp = lista.get(i);
	    lista.set(i, lista.get(j));
	    lista.set(j, temp);
	}
	
	public static boolean areEqual(List<String> list1, List<String> list2) {
	    // Null check
	    if(list1 == null || list2 == null) return false;

	    // Size check
	    if(list1.size() != list2.size()) return false;

	    // Sort and compare
	    Collections.sort(list1);
	    Collections.sort(list2);
	    return list1.equals(list2);
	}


}
