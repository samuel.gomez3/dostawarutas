package co.com.pragma.json;


import lombok.Data;

@Data
public class Bounds {
    private Location northeast;
    private Location southwest;
}
