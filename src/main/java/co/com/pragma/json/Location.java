package co.com.pragma.json;

import lombok.Data;

@Data
public class Location {
    private double lat;
    private double lng;
}
