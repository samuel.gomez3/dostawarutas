package co.com.pragma.json;

import java.util.List;

import lombok.Data;
@Data
public class Leg {
    private DistanceDuration distance;
    private DistanceDuration duration;
    private String end_address;
    private Location end_location;
    private String start_address;
    private Location start_location;
    private List<Step> steps;
    private List<Object> traffic_speed_entry;
    private List<Object> via_waypoint;
}
