package co.com.pragma.json;

import lombok.Data;

@Data
public class DistanceDuration {
    private String text;
    private int value;
}
