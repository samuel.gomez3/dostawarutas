package co.com.pragma.json;

import lombok.Data;

@Data
public class Step {
    private DistanceDuration distance;
    private DistanceDuration duration;
    private Location end_location;
    private String html_instructions;
    private String maneuver;
    private Polyline polyline;
    private Location start_location;
    private String travel_mode;
}
