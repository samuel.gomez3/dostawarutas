package co.com.pragma.json;

import java.util.List;

import lombok.Data;
@Data
public class Route {
    private Bounds bounds;
    private String copyrights;
    private List<Leg> legs;
    private Polyline overview_polyline;
    private String summary;
    private List<String> warnings;
    private List<Integer> waypoint_order;
}
