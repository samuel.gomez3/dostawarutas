package co.com.pragma.json;

import java.util.List;

import lombok.Data;
@Data
public class Root {
    private List<GeoCodedWaypoint> geocoded_waypoints;
    private List<Route> routes;
    private String status;
}
