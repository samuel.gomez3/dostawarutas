package co.com.pragma.json;

import lombok.Data;

@Data
public class Polyline {
	private String points;
}
