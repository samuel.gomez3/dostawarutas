package co.com.pragma.json;

import java.util.List;

import lombok.Data;


@Data
public class GeoCodedWaypoint {
	
    private String geocoder_status;
    private String place_id;
    private Boolean partial_match;
    private List<String> types;

}
