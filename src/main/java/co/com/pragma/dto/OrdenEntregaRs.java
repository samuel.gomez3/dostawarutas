package co.com.pragma.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrdenEntregaRs {
	
	private Object ordenEntrega;
	private Object distanciasMetros;
	private Object instrucciones;
	private Object duraccionSegundos;
	

}
