package co.com.pragma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DostawaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DostawaApplication.class, args);
	}

}
