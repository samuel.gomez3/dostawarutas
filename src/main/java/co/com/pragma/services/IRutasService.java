package co.com.pragma.services;

import java.io.IOException;

import co.com.pragma.dto.DireccionesRq;

public interface IRutasService {
	public Object obtenerDetallesRutas(DireccionesRq direcciones, String apiKey, String returg, String duration, String acopio);

	public String getPostalCodeFromAddress(String address, String apiKey) throws IOException;
}
