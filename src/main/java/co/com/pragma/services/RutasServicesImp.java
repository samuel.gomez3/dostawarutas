package co.com.pragma.services;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import co.com.pragma.dto.DireccionesRq;
import co.com.pragma.dto.OrdenEntregaRs;
import co.com.pragma.json.Leg;
import co.com.pragma.json.Root;
import co.com.pragma.json.Step;
import co.com.pragma.util.Constantes;
import co.com.pragma.util.UtilRutas;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Service
public class RutasServicesImp implements IRutasService {

	private static OkHttpClient client = new OkHttpClient();
	private static Gson gson = new Gson();

	public Object obtenerDetallesRutas(DireccionesRq direcciones, String apiKey, String returg, String duration,
			String acopio) {
		List<String> addresses = direcciones.getDirecciones();
		

		Boolean booleanReturg = Boolean.parseBoolean(returg);
		Boolean booleanDuration = Boolean.parseBoolean(duration);
		Boolean booleanAcopio = Boolean.parseBoolean(acopio);
		
		if (booleanAcopio) {
			addresses.add(0, direcciones.getAcopio());
		}

		final int SIZE = 6;

		List<List<String>> subLists = new ArrayList<>();

		for (int i = 0; i < addresses.size(); i += SIZE) {
			// Crea sublistas y las agrega a la lista de sublistas
			subLists.add(addresses.subList(i, Math.min(i + SIZE, addresses.size())));
		}

		List<List<String>> todasPermutaciones = new ArrayList<>();

		for (List<String> subList : subLists) {
			List<List<String>> permutacionesIniciales = UtilRutas.filtrarPermutacionesIniciales(subList,
					subList.get(0));

			int index = 0;

			while (permutacionesIniciales.size() < 120 && subLists.size() != 1) {
				permutacionesIniciales.add(permutacionesIniciales.get(index));
				index = (index + 1) % permutacionesIniciales.size();
			}

			if (!todasPermutaciones.isEmpty()) {

				for (int i = 0; i < todasPermutaciones.size(); i++) {
					List<String> permutacion = todasPermutaciones.get(i);
					permutacion.addAll(permutacionesIniciales.get(i));
				}

			} else {

				todasPermutaciones.addAll(permutacionesIniciales);
			}
		}

		List<Object[]> objects = new ArrayList<>();
		List<OrdenEntregaRs> ordenEntregaRs = new ArrayList<>();
		int indice = 1;
		for (List<String> permutacion : todasPermutaciones) {
			Object[] ruta = new Object[4];
			if (booleanReturg) {

				permutacion.add(direcciones.getAcopio());
			}


			System.out.println(indice + permutacion.toString());
			indice++;
			ruta[0] = permutacion;
			Root root = null;

			try {
				String origin = URLEncoder.encode(permutacion.get(0), StandardCharsets.UTF_8.toString());
				String destination = URLEncoder.encode(permutacion.get(permutacion.size() - 1),
						StandardCharsets.UTF_8.toString());
				String waypoints = permutacion.subList(1, permutacion.size() - 1).stream().map(address -> {
					try {
						return URLEncoder.encode(address, StandardCharsets.UTF_8.toString());
					} catch (Exception ex) {
						ex.printStackTrace();
						return "";
					}
				}).collect(Collectors.joining("|"));

				OkHttpClient client = new OkHttpClient();
				String urlRequest = Constantes.DIRECTIONS_API_BASE + "?origin=" + origin + "&destination=" + destination
						+ "&waypoints=optimize:true|" + waypoints + "&key=" + apiKey;

				Request request = new Request.Builder().url(urlRequest).build();

				try (Response response = client.newCall(request).execute()) {
					String jsonResponse = response.body().string();

					root = gson.fromJson(jsonResponse, Root.class);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			List<Leg> legs = root.getRoutes().get(0).getLegs();
			int totalMetros = 0;
			int totalSegundos = 0;
			List<String> instrucciones = new ArrayList<>();

			for (int i = 0; i < legs.size(); i++) {

				Leg leg = legs.get(i);
				totalMetros += leg.getDistance().getValue();
				totalSegundos += leg.getDuration().getValue();

				for (Step step : leg.getSteps()) {
					Document document = Jsoup.parse(step.getHtml_instructions());
					String textoSinEtiquetas = document.text();
					instrucciones.add(textoSinEtiquetas);
				}

			}

			ruta[1] = totalMetros;
			ruta[2] = instrucciones;
			ruta[3] = totalSegundos;
			objects.add(ruta);
		}

		Collections.sort(objects, new Comparator<Object[]>() {
			public int compare(Object[] o1, Object[] o2) {
				Integer index1 = booleanDuration ? (Integer) o1[3] : (Integer) o1[1];
				Integer index2 = booleanDuration ? (Integer) o2[3] : (Integer) o2[1];
				return index1.compareTo(index2);
			}
		});

		for (Object[] object : objects) {
			ordenEntregaRs.add(new OrdenEntregaRs(object[0], object[1], object[2], object[3]));
		}

		return ordenEntregaRs;

	}

	public String getPostalCodeFromAddress(String address, String apiKey) throws IOException {
		String url = Constantes.GEOCODING_BASE_URL + URLEncoder.encode(address, StandardCharsets.UTF_8.toString())
				+ "&key=" + apiKey;
		Request request = new Request.Builder().url(url).build();
		try (Response response = client.newCall(request).execute()) {
			String jsonData = response.body().string();
			JsonObject jsonObject = gson.fromJson(jsonData, JsonObject.class);
			JsonArray resultsArray = jsonObject.getAsJsonArray("results");
			if (resultsArray.size() > 0) {
				JsonArray addressComponents = resultsArray.get(0).getAsJsonObject()
						.getAsJsonArray("address_components");
				for (JsonElement componentElement : addressComponents) {
					JsonObject component = componentElement.getAsJsonObject();
					JsonArray types = component.getAsJsonArray("types");
					for (JsonElement typeElement : types) {
						if (typeElement.getAsString().equals("postal_code")) {
							return component.get("long_name").getAsString();
						}
					}
				}
			}
			throw new RuntimeException("Postal code not found");
		}
	}
}
