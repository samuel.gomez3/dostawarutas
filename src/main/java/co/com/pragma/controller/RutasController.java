package co.com.pragma.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.dto.DireccionesRq;

import co.com.pragma.services.IRutasService;
import lombok.RequiredArgsConstructor;

import java.io.IOException;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class RutasController {

	private final IRutasService rutasService;

	@PostMapping("/ordenEntrega")
	public Object ordenEntrega(@RequestHeader("apiKey") String apiKey,
			@RequestParam(value = "return", required = false, defaultValue = "false") String returg,
			@RequestParam(value = "acopio", required = false, defaultValue = "false") String acopio,
			@RequestParam(value = "duration", required = false, defaultValue = "false") String duration,
			@RequestBody DireccionesRq direcciones) {

		return rutasService.obtenerDetallesRutas(direcciones, apiKey, returg, duration, acopio);
	}

	@PostMapping("/codigoPostal")
	public Object codigoPostal(@RequestHeader("apiKey") String apiKey, @RequestBody DireccionesRq direcciones) {
		try {
			return rutasService.getPostalCodeFromAddress(direcciones.getDirecciones().get(0), apiKey);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return direcciones.getDirecciones().get(0);
	}

}
